Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fcitx-googlepinyin
Source: https://download.fcitx-im.org/fcitx-anthy/

Files: *
Copyright: 2012, CSSlayer <wengxt@gmail.com>
License: GPL-2+

Files: profile/101kana.sty profile/canna.sty profile/atok.sty profile/azik.sty
       profile/msime.sty profile/vje-delta.sty profile/wnn.sty src/action.*
Copyright: 2005, Takuro Ashie <ashie@homa.ne.jp>
License: GPL-2+

Files: src/conversion.* src/default_tables.* src/kana.* src/reading.* src/style_file.* src/utils.*
Copyright: 2004, 2005, Takuro Ashie <ashie@homa.ne.jp>
           2012, CSSlayer <wengxt@gmail.com>
License: GPL-2+

Files: src/key2kana* src/nicola.*
Copyright: 2004, 2005, Hiroyuki Ikezoe <poincare@ikezoe.net>
           2004, 2005, Takuro Ashie <ashie@homa.ne.jp>
           2012, CSSlayer <wengxt@gmail.com>
License: GPL-2+

Files: src/factory.* src/imengine.*
Copyright: 2004, 2005, Hiroyuki Ikezoe <poincare@ikezoe.net>
           2004, 2005, Takuro Ashie <ashie@homa.ne.jp>
           2004, James Su <suzhe@tsinghua.org.cn>
           2012, CSSlayer <wengxt@gmail.com>
License: GPL-2+

Files: src/preedit.cpp src/action.h
Copyright: 2004, 2005, Takuro Ashie <ashie@homa.ne.jp>
           2012, CSSlayer <wengxt@gmail.com>
License: GPL-2+

Files: src/action.cpp
Copyright: 2005, Takuro Ashie <ashie@homa.ne.jp>
License: GPL-2+

Files: profile/nicola-a.sty profile/nicola-j.sty profile/oasys100j.sty
Copyright: 2005, Hatuka*nezumi <nezumi@jca.apc.org>
License: GPL-2+

Files: profile/nicola-f.sty
Copyright: 2005, Hatuka*nezumi <nezumi@jca.apc.org>
           2005, MORIYAMA Masayuki <msyk@mtg.biglobe.ne.jp>
License: GPL-2+

Files: profile/tron-dvorak.sty profile/tron-qwerty-jp.sty
Copyright: 2005, Okano Shinchi <shinchan.okano@nifty.com>
License: GPL-2+

Files: profile/tsuki-2-203-101.sty profile/tsuki-2-203-106.sty
Copyright: 2006, Tatsuki Sugiura <sugi@nemui.org>
License: GPL-2+

Files: debian/*
Copyright: 2010, Aron Xu <aron@debian.org>
           2018, Boyuan Yang <073plan@gmail.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA.
 .
 On Debian systems, the full text of GPL-2 could be found at
 `/usr/share/common-licenses/GPL-2'.
